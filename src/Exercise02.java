import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {
        System.out.print("Enter the ambient temperature: ");
        int temperature = new Scanner(System.in).nextInt();
        System.out.print("Enter the wind speed: ");
        double speedWind = new Scanner(System.in).nextDouble();
        System.out.print("Is it raining outside? (yes/no): ");
        String rain = new Scanner(System.in).nextLine();

        System.out.println(recommendations(temperature, speedWind, rain));
    }

    private static String recommendations(int temperature, double speedWind, String rain) {
        if (15 <= temperature && temperature <= 22) {
            if (0 <= speedWind && speedWind < 10) {
                if (rain.equals("no") || rain.equals("NO") || rain.equals("N") || rain.equals("No")) {
                    return "The current weather conditions are suitable for walking";
                }
            }
        }
        return "The current weather conditions are not suitable for walking";
    }
}
